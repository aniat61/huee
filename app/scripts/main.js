$(document).ready(function () {
    $('.slider--doctors').slick({
        dots: false,
        arrow: true,
        autoplay: false,
        autoplaySpeed: 2000,
        nextArrow: '<i class="next"></i>',
        prevArrow: '<i class="prev"></i>',
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }]
    });
    $('.slider--prices').slick({
        dots: false,
        arrow: true,
        autoplay: false,
        autoplaySpeed: 2000,

        slidesToShow: 5,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    nextArrow: '<i class="next"></i>',
                    prevArrow: '<i class="prev"></i>',
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    nextArrow: '<i class="next"></i>',
                    prevArrow: '<i class="prev"></i>',
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                     nextArrow: '<i class="next"></i>',
                    prevArrow: '<i class="prev"></i>',
                }
            },
        
        ]
    });
    $('.slider--reviews').slick({
        dots: false,
        arrow: true,
        autoplay: false,
        autoplaySpeed: 2000,

        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    nextArrow: '<i class="next"></i>',
                    prevArrow: '<i class="prev"></i>',
                }
            },
            
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                     nextArrow: '<i class="next"></i>',
                    prevArrow: '<i class="prev"></i>',
                }
            },
        
        ]
    });
});
